package com.example.randomdavaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }



    private fun init(){
        Button.setOnClickListener {
            d("Button", "Button Activated")
            text()
        }
    }

    private fun text() {
        val number: Int = (-100..100).random()
        d("randomcifri", "$number")
        if (number % 5 == 0 )
            if (number / 5 > 0)
                text.text = "Yes"

        else
                text.text="No"
    }
}